class AvoidMockRule {

    int aField;

    @MyAnnotation
    public void aMethod() {
    }

    @Zuper // Noncompliant {{Avoid using annotation @Zuper}}
    public void anotherMethod() {
    }

    @other.Zuper // Compliant
    public void aThirdMethod() {
    }
}