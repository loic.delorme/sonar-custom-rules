package org.sonar.malt.java;

import org.junit.jupiter.api.Test;
import org.sonar.api.*;
import org.sonar.api.utils.Version;

import static org.assertj.core.api.Assertions.*;
import static org.sonar.api.SonarEdition.COMMUNITY;
import static org.sonar.api.SonarProduct.SONARQUBE;
import static org.sonar.api.SonarQubeSide.SCANNER;

class MockitoRulesPluginTest {

    @Test
    void verifyExtensions() {
        // GIVEN
        final Plugin.Context context = new Plugin.Context(new InMemorySonarRuntime());

        // WHEN
        new MockitoRulesPlugin().define(context);

        // THEN
        assertThat(context.getExtensions()).hasSize(2);
    }

    private static class InMemorySonarRuntime implements SonarRuntime {

        @Override
        public Version getApiVersion() {
            return Version.create(7, 9);
        }

        @Override
        public SonarProduct getProduct() {
            return SONARQUBE;
        }

        @Override
        public SonarQubeSide getSonarQubeSide() {
            return SCANNER;
        }

        @Override
        public SonarEdition getEdition() {
            return COMMUNITY;
        }
    }
}