package org.sonar.malt.java.rules;

import org.junit.jupiter.api.Test;

import static org.sonar.java.checks.verifier.CheckVerifier.newVerifier;

class AvoidMockRuleTest {

    @Test
    void verifyAvoidMockRule() {
        // GIVEN
        final AvoidMockRule rule = new AvoidMockRule();

        // WHEN
        newVerifier()
                .onFile("src/test/files/AvoidMockRule.java")
                .withCheck(rule)
                .verifyNoIssues();

        // THEN
    }
}