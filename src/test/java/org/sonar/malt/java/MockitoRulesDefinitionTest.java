package org.sonar.malt.java;

import org.junit.jupiter.api.Test;
import org.sonar.api.server.rule.RulesDefinition;
import org.sonar.api.server.rule.RulesDefinition.Repository;
import org.sonar.api.server.rule.RulesDefinition.Rule;

import static org.assertj.core.api.Assertions.*;
import static org.sonar.api.rule.RuleStatus.READY;
import static org.sonar.api.rules.RuleType.CODE_SMELL;
import static org.sonar.api.server.debt.DebtRemediationFunction.Type.CONSTANT_ISSUE;
import static org.sonar.malt.java.MockitoRules.getAllRules;
import static org.sonar.malt.java.MockitoRulesDefinition.REPOSITORY_KEY;
import static org.sonar.malt.java.MockitoRulesDefinition.REPOSITORY_NAME;

class MockitoRulesDefinitionTest {

    @Test
    void verifyRepository() {
        // GIVEN
        final RulesDefinition.Context context = new RulesDefinition.Context();
        final MockitoRulesDefinition rulesDefinition = new MockitoRulesDefinition();
        final RulesDefinition.Repository repository = context.repository(REPOSITORY_KEY);

        // WHEN
        rulesDefinition.define(context);

        // THEN
        assertThat(repository.key()).isEqualTo(REPOSITORY_KEY);
        assertThat(repository.name()).isEqualTo(REPOSITORY_NAME);
        assertThat(repository.language()).isEqualTo("java");
        assertThat(repository.rules()).hasSize(getAllRules().size());
        assertThat(repository.rules().stream().filter(Rule::template)).isEmpty();

        assertRuleProperties(repository);
    }

    private static void assertRuleProperties(Repository repository) {
        final Rule rule = repository.rule("AvoidMock");
        assertThat(rule).isNotNull();
        assertThat(rule.name()).isEqualTo("TODO");
        assertThat(rule.type()).isEqualTo(CODE_SMELL);
        assertThat(rule.status()).isEqualTo(READY);
        assertThat(rule.debtRemediationFunction().type()).isEqualTo(CONSTANT_ISSUE);
        assertThat(rule.severity()).isEqualTo("Minor");
    }
}