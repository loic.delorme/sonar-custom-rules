package org.sonar.malt.java;

import org.junit.jupiter.api.Test;
import org.sonar.plugins.java.api.CheckRegistrar;

import static org.assertj.core.api.Assertions.*;

class MockitoRulesCheckRegistrarTest {

    @Test
    void verifyContext() {
        // GIVEN
        final CheckRegistrar.RegistrarContext context = new CheckRegistrar.RegistrarContext();
        final MockitoRulesCheckRegistrar registrar = new MockitoRulesCheckRegistrar();

        // WHEN
        registrar.register(context);

        // THEN
        assertThat(context.checkClasses()).hasSize(0);
        assertThat(context.testCheckClasses()).hasSize(1);
    }
}