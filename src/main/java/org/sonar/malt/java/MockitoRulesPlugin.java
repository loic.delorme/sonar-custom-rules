package org.sonar.malt.java;

import org.sonar.api.Plugin;

public final class MockitoRulesPlugin implements Plugin {

    @Override
    public void define(Context context) {
        // server extensions -> objects are instantiated during server startup
        context.addExtension(MockitoRulesDefinition.class);

        // batch extensions -> objects are instantiated during code analysis
        context.addExtension(MockitoRulesCheckRegistrar.class);
    }
}