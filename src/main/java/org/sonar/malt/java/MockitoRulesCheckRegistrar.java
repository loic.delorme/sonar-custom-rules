package org.sonar.malt.java;

import org.sonar.plugins.java.api.CheckRegistrar;
import org.sonarsource.api.sonarlint.SonarLintSide;

import static org.sonar.malt.java.MockitoRules.getJavaRules;
import static org.sonar.malt.java.MockitoRules.getJavaTestRules;
import static org.sonar.malt.java.MockitoRulesDefinition.REPOSITORY_KEY;

@SonarLintSide
public final class MockitoRulesCheckRegistrar implements CheckRegistrar {

    @Override
    public void register(RegistrarContext context) {
        context.registerClassesForRepository(
                REPOSITORY_KEY,
                getJavaRules(),
                getJavaTestRules()
        );
    }
}