package org.sonar.malt.java;

import org.sonar.api.server.rule.RulesDefinition;
import org.sonarsource.analyzer.commons.RuleMetadataLoader;

import java.util.ArrayList;

import static org.sonar.malt.java.MockitoRules.getAllRules;

public final class MockitoRulesDefinition implements RulesDefinition {

    private static final String RESOURCE_BASE_PATH = "org/sonar/l10n/java/rules/java";

    public static final String REPOSITORY_KEY = "malt-java";
    public static final String REPOSITORY_NAME = "Malt Mockito Repository";

    @Override
    public void define(Context context) {
        final NewRepository repository = context
                .createRepository(REPOSITORY_KEY, "java")
                .setName(REPOSITORY_NAME);

        final RuleMetadataLoader ruleMetadataLoader = new RuleMetadataLoader(RESOURCE_BASE_PATH);
        ruleMetadataLoader.addRulesByAnnotatedClass(repository, new ArrayList<>(getAllRules()));

        repository.done();
    }
}