package org.sonar.malt.java;

import org.sonar.malt.java.rules.AvoidMockRule;
import org.sonar.plugins.java.api.JavaCheck;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.unmodifiableList;

public final class MockitoRules {

    private MockitoRules() {
    }

    public static List<Class<? extends JavaCheck>> getAllRules() {
        final List<Class<? extends JavaCheck>> allRules = new ArrayList<>();
        allRules.addAll(getJavaRules());
        allRules.addAll(getJavaTestRules());

        return unmodifiableList(allRules);
    }

    public static List<Class<? extends JavaCheck>> getJavaRules() {
        return emptyList();
    }

    public static List<Class<? extends JavaCheck>> getJavaTestRules() {
        return asList(AvoidMockRule.class);
    }
}