package org.sonar.malt.java.rules;

import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;
import org.sonar.check.Rule;
import org.sonar.plugins.java.api.JavaFileScanner;
import org.sonar.plugins.java.api.JavaFileScannerContext;
import org.sonar.plugins.java.api.tree.BaseTreeVisitor;
import org.sonar.plugins.java.api.tree.MethodTree;

@Rule(key = "AvoidMockRule")
public class AvoidMockRule extends BaseTreeVisitor implements JavaFileScanner {

    private static final Logger LOGGER = Loggers.get(AvoidMockRule.class);

    private JavaFileScannerContext context;

    @Override
    public void scanFile(JavaFileScannerContext context) {
        this.context = context;
        scan(context.getTree());
    }

    @Override
    public void visitMethod(MethodTree tree) {
        // when(accountService.find.*(any(AccountId.class))).thenAnswer(invocation -> Optional.of(account));
        super.visitMethod(tree);
    }
}